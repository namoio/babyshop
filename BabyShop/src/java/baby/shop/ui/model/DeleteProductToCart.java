/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.ui.model;

import baby.shop.biz.Cart;
import baby.shop.entity.Product;
import static com.opensymphony.xwork2.Action.ERROR;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nam oio
 */
public class DeleteProductToCart extends ActionSupport {
    
    public DeleteProductToCart() {
    }
     private int newProductId;
    
    public void setNewProductId(int newProductId){
        this.newProductId=newProductId;
    }
    public String execute() throws Exception {
        delete();
        return SUCCESS;
    }
        public boolean delete(){
              Cart cart=(Cart) ActionContext.getContext().getSession().get("cart");
       
       
                
                Map<Product,Integer> m= cart.getproduct();
             Set<Product> keySet =m.keySet();
           
         
           
              for (Product key : m.keySet()) {
    if (key.getId()==newProductId) {
        
        cart.delete(key);
    }
}
            
     
                
            return true;
                
            }
}
