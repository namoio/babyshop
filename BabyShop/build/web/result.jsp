<%-- 
    Document   : result
    Created on : May 3, 2018, 4:45:45 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product list of search <s:property value="keyword"/></title>
    </head>
    <body>
        <table border="1">
        <s:iterator value="product" var="product">
        <tr>
            
            <td><s:property value="name"/></td>
             <td><s:property value="price"/></td>
              <td><s:property value="description"/></td>
              <td><a href="addToCart?newProductId=<s:property value="id"/>">Add to cart</a></td>
        </tr>
        </s:iterator>
        </table>
        <a href="index.jsp">Back</a>
    </body>
</html>
