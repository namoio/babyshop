<%-- 
    Document   : ViewCart
    Created on : May 3, 2018, 4:46:31 PM
    Author     : nam oio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart Detail</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>Product</th>
                
                <th>Amount</th>
                <th>+</th>
                <th>-</th>
            </tr>
            <s:iterator value="products" var="product">
                <tr>
                    <td>
                        <s:property value="key.name"/>
                       
                      
                    </td>
                    
                    <td>  <s:property value="value"/></td>
                    <td><a href="addToCart?newProductId=<s:property value="key.id"/>">add</a></td>
                    <td><a href="minus?newProductId=<s:property value="key.id"/>">minus</a></td>
                    <td><a href="delete?newProductId=<s:property value="key.id"/>">delete</a></td>
                </tr>
            </s:iterator>
        </table>
        <label>Total:</label><s:property value="total"/><br/>
        <a href="index.jsp">Contiue buying</a>
    </body>
</html>
